package ucr.ac.ecci.ci1322.algorithmsTest;
import java.util.Random;

public class sortAlgorthm {


    public sortAlgorthm(){
    }

    public int[] fillVector(){
        int [] vector = new int[8];
        Random rand = new Random();
        for(int item = 0; item < vector.length; item++)
        {
            vector[item] = 1+rand.nextInt((10));
        }

        return vector;
    }

    public void showContentVector(int[] vector){
        for(int item : vector)
        {
            System.out.print(item+", ");
        }
        System.out.println();
    }

    public void mergeSort(int leftIndex, int rightIndex, int[] vector)
    {
        if(leftIndex < rightIndex)
        {
            mergeSort(leftIndex, (leftIndex + rightIndex)/2, vector);
            mergeSort((leftIndex + rightIndex)/2 + 1, rightIndex, vector);  //PRECAUCIÓN CON VECTORES MAYORES A 10, HACER CAMBIO A VARIABLE DE FINAL DEL SEGUNDA LISTA
            sort(leftIndex, rightIndex, vector);
        }
    }

    public void sort(int lefterItem, int righterItem, int[] vector)
    {
        int[] auxVector = new int[righterItem-lefterItem + 1];             //for the middle sort int the main vector
        int left = lefterItem;
        int center = (lefterItem + righterItem)/2 + 1;
        int auxIndex = 0;

        //ciclo controla 2 indices: left representa al inidice que recorre el principio de la parte de la lista que se va a ordenar
        //mientras que ccenter representa la segunda mitad de la parte de la lista que se va a ordenar.
        while( left < (lefterItem + righterItem)/2 + 1 && center <= righterItem)
        {
            if(vector[left] <= vector[center]){
                auxVector[auxIndex] = vector[left];
                left++;
            } else {
                auxVector[auxIndex] = vector[center];
                center++;
            }
            auxIndex++;
        }

        /*Si aún no ha terminado con la primera mitad, pero sí con la segunda; entonces la termina de pasar al
        * vector auxiliar lo que quedó de la mitad que no se repasó al completo*/
        while(left < (lefterItem + righterItem)/2 + 1)
        {
            auxVector[auxIndex] = vector[left];
            left++;
            auxIndex++;
        }

        /*Si aún no ha terminado con la segunda mitad, pero sí con la primera; entonces la termina de pasar al
         * vector auxiliar lo que quedó de la mitad que no se repasó al completo*/
        while(center <= righterItem)
        {
            auxVector[auxIndex] = vector[center];
            center++;
            auxIndex++;
        }

        int indexToPass = lefterItem;

        for(int i = 0; i < righterItem - lefterItem +1; i++)
        {
            vector[indexToPass] = auxVector[i];
            indexToPass++;
        }
    }

    public static void main (String[] args){
        sortAlgorthm alg = new sortAlgorthm();

        int[] v = alg.fillVector();

        alg.showContentVector(v);

        alg.mergeSort(0, v.length-1, v);

        alg.showContentVector(v);

    }
}
