package ucr.ac.ecci.ci1322.algorithmsTest;
import javax.swing.*;

public class graphics extends JFrame{
    JPanel panel;
    JButton button;
    JLabel label;

    public graphics(){
        panel = new JPanel();
        button = new JButton();
        label = new JLabel();

        this.add(panel);
        panel.add(button);
        panel.add(label);

        label.setText("Prueba");
        button.setText("boton");
    }

    public void makeWindow(){
        this.setVisible(true);
        this.setBounds(400, 500, 200, 200);
    }

    public static void main (String[]args){
        graphics g = new graphics();
        g.makeWindow();
    }
}
