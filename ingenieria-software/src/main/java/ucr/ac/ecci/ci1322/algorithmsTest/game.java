package ucr.ac.ecci.ci1322.algorithmsTest;
import javax.swing.*;
import java.util.*;


public class game {
    private boolean is2048;
    private String mainArray [][];
    private int size = 4;

    public game() {
        this.is2048 = false;

    }

    public void setArraySize(){
        if(JOptionPane.showConfirmDialog(null, "¿Desea que el tamano de la matriz se de 4x4?", "Solicitud de confirmacion",JOptionPane.YES_NO_OPTION) == 0){
            this.mainArray = new String[this.size][this.size];
        } else {
            System.out.println("Introduzca el tamano para la matriz");
            Scanner scnn = new Scanner(System.in);
            System.out.print("Tamano: ");
            this.size = Integer.parseInt(scnn.nextLine());
            this.mainArray = new String[this.size][this.size];
        }
    }

    public void showArray(){
        for(int i = 0; i < this.size; i++){
            for (int h = 0; h < this.size; h++){
                System.out.print(" ___ ");
            }
            System.out.println();

            for(int j = 0; j < this.size; j++){
                System.out.print("| " + this.mainArray[i][j] + " |");
            }
            System.out.println();
        }
        for (int h = 0; h < this.size; h++){
            System.out.print(" ___ ");
        }
    }

    public void fillArray(){
        for(int i = 0; i < this.size; i++){
            for(int j = 0; j < this.size; j++){
                this.mainArray[i][j] = " ";
            }
        }
    }

    public void mainController(){
        setArraySize();
        fillArray();
        getRandomAceptedPosition();
        getRandomAceptedPosition();
        showArray();

        while(gameContinue()){
            selectedMovement();
            getRandomAceptedPosition();
            showArray();
        }
    }

    public boolean gameContinue(){
        boolean continueTheGame = false;
        for(int i = 0; i < this.size; i++) {
            for (int j = 0; j < this.size; j++) {
                if(this.mainArray[i][j] == " "){
                    continueTheGame = true;
                } else if(i-1 >= 0){          //verifica arriba
                    if(this.mainArray[i-1][j].equals(this.mainArray[i][j])){
                        continueTheGame = true;
                    }
                } else if(i+1 <= 3){          //verifica abajo.
                    if(this.mainArray[i+1][j].equals(this.mainArray[i][j])){
                        continueTheGame = true;
                    }
                } else if(j-1 >= 0){          //verifica izquierda.
                    if(this.mainArray[i][j-1].equals(this.mainArray[i][j])){
                        continueTheGame = true;
                    }
                } else if(j+1 <= 3){          //verifica derecha.
                    if(this.mainArray[i][j+1].equals(this.mainArray[i][j])){
                        continueTheGame = true;
                    }
                }
            }
        }
        if(!continueTheGame){
            JOptionPane.showInternalMessageDialog(null, "Has perdido.");
        }
        return continueTheGame;
    }

    public String getNewRandomNumber(){
        Random rand = new Random();
        int randomNumber = rand.nextInt(99);
        String numberToReturn;
        if(randomNumber >= 0 && randomNumber < 74){
            numberToReturn = "2";
        } else {
            numberToReturn = "4";
        }
        return numberToReturn;
    }

    public void getRandomAceptedPosition(){
        boolean findedAceptedPosition = false;
        Random rand = new Random();
        int randPositionInAxiX;
        int randPositionInAxiY;

        while(!findedAceptedPosition && gameContinue()){
            randPositionInAxiX = rand.nextInt(this.size);
            randPositionInAxiY = rand.nextInt(this.size);
            if(this.mainArray[randPositionInAxiX][randPositionInAxiY] == " "){
                this.mainArray[randPositionInAxiX][randPositionInAxiY] = getNewRandomNumber();
                findedAceptedPosition = true;
            }
        }
    }


    public void moveUp(){
        String auxiliarValue;
        int calculateValue;
        for(int i = 1; i < this.size; i++) {              //comienza en 1 porque la fila 0 es el límite y esa no se puede pasar
            for (int j = 0; j < this.size; j++) {
                if(this.mainArray[i][j] != " "){
                    for(int begin = i; begin > 0 && (this.mainArray[begin-1][j].equals(" ") || this.mainArray[begin][j].equals(this.mainArray[begin-1][j])); begin--){
                        if (this.mainArray[begin][j].equals(this.mainArray[begin-1][j])){
                            calculateValue = 2*Integer.parseInt(this.mainArray[begin-1][j]);
                            this.mainArray[begin-1][j] = Integer.toString(calculateValue);
                            this.mainArray[begin][j] = " ";
                        } else {
                            auxiliarValue = this.mainArray[begin][j];
                            this.mainArray[begin][j] = " ";
                            this.mainArray[begin-1][j] = auxiliarValue;
                        }
                    }
                }
            }
        }
    }

    public void moveDown(){      //Recorre la matriz de abajo hacia arriba.
        String auxiliarValue;
        int calculateValue;
        for(int i = this.size-2; i >= 0 ; i--) {
            for (int j = this.size-1; j >= 0; j--) {
                if(this.mainArray[i][j] != " "){
                    for(int begin = i; begin < this.size-1 && (this.mainArray[begin+1][j].equals(" ") || this.mainArray[begin][j].equals(this.mainArray[begin+1][j])); begin++){
                        if (this.mainArray[begin][j].equals(this.mainArray[begin+1][j])){
                            calculateValue = 2*Integer.parseInt(this.mainArray[begin+1][j]);
                            this.mainArray[begin+1][j] = Integer.toString(calculateValue);
                            this.mainArray[begin][j] = " ";
                        } else {
                            auxiliarValue = this.mainArray[begin][j];
                            this.mainArray[begin][j] = " ";
                            this.mainArray[begin+1][j] = auxiliarValue;
                        }
                    }
                }
            }
        }
    }

    public void moveRight(){
        String auxiliarValue;
        int calculateValue;
        for(int i = 0; i < this.size; i++) {
            for (int j = this.size-2; j >= 0; j--) {
                if(!this.mainArray[i][j].equals(" ")){
                    for(int begin = j; begin < this.size-1 && (this.mainArray[i][begin+1].equals(" ") || this.mainArray[i][begin].equals(this.mainArray[i][begin+1])); begin++){
                        if (this.mainArray[i][begin].equals(this.mainArray[i][begin+1])){
                            calculateValue = 2*Integer.parseInt(this.mainArray[i][begin+1]);
                            this.mainArray[i][begin+1] = Integer.toString(calculateValue);
                            this.mainArray[i][begin] = " ";
                        } else {
                            auxiliarValue = this.mainArray[i][begin];
                            this.mainArray[i][begin] = " ";
                            this.mainArray[i][begin+1] = auxiliarValue;
                        }
                    }
                }
            }
        }
    }

    public void moveLeft(){
        String auxiliarValue;
        int calculateValue;
        for(int i = 0; i < this.size; i++) {
            for (int j = 1; j < this.size; j++) {
                if(this.mainArray[i][j] != " "){
                    for(int begin = j; begin > 0 && (this.mainArray[i][begin-1].equals(" ") || this.mainArray[i][begin].equals(this.mainArray[i][begin-1])); begin--){
                        if (this.mainArray[i][begin].equals(this.mainArray[i][begin-1])){
                            calculateValue = 2*Integer.parseInt(this.mainArray[i][begin-1]);
                            this.mainArray[i][begin-1] = Integer.toString(calculateValue);
                            this.mainArray[i][begin] = " ";
                        } else {
                            auxiliarValue = this.mainArray[i][begin];
                            this.mainArray[i][begin] = " ";
                            this.mainArray[i][begin-1] = auxiliarValue;
                        }
                    }
                }
            }
        }
    }

    public void selectedMovement() {
        int movement;
        Scanner scnn = new Scanner(System.in);
        System.out.print("\ndSeleccione siguiente movimiento: ");
        movement = scnn.nextLine().charAt(0);
        System.out.print("\ndSeleccione siguiente movimiento: " + movement);
        switch(movement){
            case 115:
                moveDown();
                System.out.println("\n\n");
                //showArray();
                break;

            case 97:
                moveLeft();
                System.out.println("\n\n");
                //showArray();
                break;

            case 100:
                moveRight();
                System.out.println("\n\n");
                //showArray();
                break;

            case 119:
                moveUp();
                System.out.println("\n\n");
                //showArray();
                break;
        }
    }

    public static void main (String[] args){
        game g = new game();

        g.mainController();
    }
}
